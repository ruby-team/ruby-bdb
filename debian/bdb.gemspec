
Gem::Specification.new do |spec|
  spec.name = "bdb"
  spec.version = "0.6.6"
  spec.authors = ["Akinori MUSHA", "Guy Decoux"]
  spec.email = ["knu@idaemons.org", nil]
  spec.description = %q{This is a Ruby interface to Berkeley DB >= 2.0.}
  spec.summary = %q{A Ruby interface to Berkeley DB >= 2.0}
  spec.homepage = "https://github.com/knu/ruby-bdb"
  spec.license = "Ruby's"

  spec.extensions = ['src/extconf.rb']
end