Description: Use the RHASH_IFNONE macro instead of deprecated RHASH one

Author: David Suárez <david.sephirot@gmail.com>
Last-Update: 2015-08-14

--- a/src/common.c
+++ b/src/common.c
@@ -1303,7 +1303,7 @@ bdb_s_new(int argc, VALUE *argv, VALUE o
     if (argc && TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE v, f = argv[argc - 1];
 
-	if ((v = rb_hash_aref(f, rb_str_new2("txn"))) != RHASH(f)->ifnone) {
+	if ((v = rb_hash_aref(f, rb_str_new2("txn"))) != RHASH_IFNONE(f)) {
 	    if (!rb_obj_is_kind_of(v, bdb_cTxn)) {
 		rb_raise(bdb_eFatal, "argument of txn must be a transaction");
 	    }
@@ -1316,7 +1316,7 @@ bdb_s_new(int argc, VALUE *argv, VALUE o
 	    dbst->options |= envst->options & BDB_NO_THREAD;
 	    dbst->marshal = txnst->marshal;
 	}
-	else if ((v = rb_hash_aref(f, rb_str_new2("env"))) != RHASH(f)->ifnone) {
+	else if ((v = rb_hash_aref(f, rb_str_new2("env"))) != RHASH_IFNONE(f)) {
 	    if (!rb_obj_is_kind_of(v, bdb_cEnv)) {
 		rb_raise(bdb_eFatal, "argument of env must be an environnement");
 	    }
@@ -1330,11 +1330,11 @@ bdb_s_new(int argc, VALUE *argv, VALUE o
 #if HAVE_CONST_DB_ENCRYPT 
 	if (envst && (envst->options & BDB_ENV_ENCRYPT)) {
 	    VALUE tmp = rb_str_new2("set_flags");
-	    if ((v = rb_hash_aref(f, rb_intern("set_flags"))) != RHASH(f)->ifnone) {
+	    if ((v = rb_hash_aref(f, rb_intern("set_flags"))) != RHASH_IFNONE(f)) {
 		rb_hash_aset(f, rb_intern("set_flags"), 
 			     INT2NUM(NUM2INT(v) | DB_ENCRYPT));
 	    }
-	    else if ((v = rb_hash_aref(f, tmp)) != RHASH(f)->ifnone) {
+	    else if ((v = rb_hash_aref(f, tmp)) != RHASH_IFNONE(f)) {
 		rb_hash_aset(f, tmp, INT2NUM(NUM2INT(v) | DB_ENCRYPT));
 	    }
 	    else {
@@ -3082,8 +3082,8 @@ bdb_each_kvc(argc, argv, obj, sens, repl
 
     if (argc && TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE g, f = argv[argc - 1];
-	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH(f)->ifnone ||
-	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH(f)->ifnone) {
+	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH_IFNONE(f) ||
+	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH_IFNONE(f)) {
 	    flags = NUM2INT(g);
 	}
 	argc--;
@@ -3401,8 +3401,8 @@ bdb_clear(int argc, VALUE *argv, VALUE o
     flags = 0;
     if (argc && TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE g, f = argv[argc - 1];
-	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH(f)->ifnone ||
-	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH(f)->ifnone) {
+	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH_IFNONE(f) ||
+	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH_IFNONE(f)) {
 	    flags = NUM2INT(g);
 	}
 	argc--;
@@ -3426,8 +3426,8 @@ bdb_replace(int argc, VALUE *argv, VALUE
     flags = 0;
     if (TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE f = argv[argc - 1];
-	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH(f)->ifnone ||
-	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH(f)->ifnone) {
+	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH_IFNONE(f) ||
+	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH_IFNONE(f)) {
 	    flags = NUM2INT(g);
 	}
 	argc--;
--- a/src/cursor.c
+++ b/src/cursor.c
@@ -29,8 +29,8 @@ bdb_cursor(int argc, VALUE *argv, VALUE
     flags = 0;
     if (argc && TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE g, f = argv[argc - 1];
-	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH(f)->ifnone ||
-	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH(f)->ifnone) {
+	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH_IFNONE(f) ||
+	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH_IFNONE(f)) {
 	    flags = NUM2INT(g);
 	}
 	argc--;
--- a/src/recnum.c
+++ b/src/recnum.c
@@ -17,7 +17,7 @@ bdb_recnum_init(int argc, VALUE *argv, V
 	argc++;
     }
     rb_hash_aset(argv[argc - 1], array, INT2FIX(0));
-    if (rb_hash_aref(argv[argc - 1], sarray) != RHASH(argv[argc - 1])->ifnone) {
+    if (rb_hash_aref(argv[argc - 1], sarray) != RHASH_IFNONE(argv[argc - 1])) {
 	rb_hash_aset(argv[argc - 1], sarray, INT2FIX(0));
     }
     rb_hash_aset(argv[argc - 1], rb_str_new2("set_flags"), INT2FIX(DB_RENUMBER));
@@ -697,8 +697,8 @@ bdb_sary_clear(int argc, VALUE *argv, VA
 
     if (argc && TYPE(argv[argc - 1]) == T_HASH) {
 	VALUE f = argv[argc - 1];
-	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH(f)->ifnone ||
-	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH(f)->ifnone) {
+	if ((g = rb_hash_aref(f, rb_intern("flags"))) != RHASH_IFNONE(f) ||
+	    (g = rb_hash_aref(f, rb_str_new2("flags"))) != RHASH_IFNONE(f)) {
 	    flags = NUM2INT(g);
 	}
 	argc--;
