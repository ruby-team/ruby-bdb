#!/usr/bin/ruby

Dir.foreach('tests') do |test_file|
  next if /^\./ =~ test_file || /(_\.rb|~)$/ =~ test_file
  next if FileTest.directory?(test_file)

  system("#{ENV['RUBY_TEST_BIN']} tests/#{test_file}") || exit(1)
end
